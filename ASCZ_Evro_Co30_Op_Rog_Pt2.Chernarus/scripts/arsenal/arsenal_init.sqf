// ARSENAL
//-------------------------------------------------------------
// itemy
_asczArsenalItems1 = ["Binocular","ItemCompass","ItemGPS","ItemMap","Toolkit","ItemWatch","G_Bandanna_tan","G_Bandanna_khk","G_Bandanna_oli","rhs_balaclava","G_Balaclava_blk","G_Balaclava_oli","G_Spectacles","G_Aviator","CUP_FR_NeckScarf","CUP_FR_NeckScarf2","kae_OldGlasses","kae_RoundGlasses","rhs_scarf","kae_SunGlasses","T_HoodTan","H_Bandanna_camo","H_Bandanna_khk","H_Bandanna_cbr","H_Bandanna_sgg","H_Cap_grn","CUP_H_FR_BandanaGreen","H_Bandana_brown","H_Bandana_khaki","CUP_H_FR_BandanaWdl","rhs_beanie_green","H_Cowboy_Hat_brown","H_Booniehat_khk","H_Booniehat_oli","H_Booniehat_tan","H_ShemagOpen_tan","H_Shemag_olive","rds_Woodlander_cap2","rds_Woodlander_cap4","rds_Villager_cap2","rds_Villager_cap4","rds_Villager_cap1","rds_worker_cap4","U_I_FullGhillie_lsh","U_I_FullGhillie_sard","U_CDF_A3_SniperGhillie","CUP_U_I_Napa_Flecktarn2","CUP_U_I_Napa_Flecktarn","CUP_V_I_Carrier_Belt","V_TacVest_oli","V_I_G_resistanceLeader_F","CUP_V_B_RRV_TL","ACRE_PRC343","ACRE_PRC77","CUP_optic_PSO_1","RHS_ACC_PSO1M2","CUP_SVD_camo_g","ACE_CableTie","ACE_Banana","ACE_DAGR","ACE_Clacker","ACE_M26_Clacker","ACE_DefusalKit","ACE_DeadManSwitch","ACE_Cellphone","ACE_EarPlugs","ACE_HuntIR_monitor","ACE_Kestrel4500","ACE_UAVBattery","ACE_wirecutter","ACE_MapTools","ACE_atropine","ACE_fieldDressing","ACE_elasticBandage","ACE_quikclot","ACE_bloodIV","ACE_bloodIV_500","ACE_bloodIV_250","ACE_bodyBag","ACE_bodyBagObject","ACE_epinephrine","ACE_morphine","ACE_packingBandage","ACE_personalAidKit","ACE_plasmaIV","ACE_plasmaIV_500","ACE_plasmaIV_250","ACE_salineIV","ACE_salineIV_500","ACE_salineIV_250","ACE_surgicalKit","ACE_tourniquet","ACE_microDAGR","ACE_RangeTable_82mm","ACE_SpareBarrel","ACE_RangeCard","ACE_Sandbag_empty","ACE_Tripod","ACE_ATragMX","ACE_IR_Strobe_Item"];

// zasobniky
_asczArsenalItems2 = true;

// batohy
_asczArsenalItems3 = ["CUP_B_AlicePack_Khaki","CUP_B_CivPack_WDL","B_FieldPack_oli","rhs_rpg_empty","rhs_sidor","B_TacticalPack_oli","B_Carryall_oli","rhs_assault_umbts","CUP_B_USMC_AssaultPack","B_CDF_A3_Backpack"];

// zbrane
_asczArsenalItems4 = ["rhs_weap_akm","rhs_weap_akm_gp25","rhs_weap_akms","rhs_weap_akms_gp25","CUP_srifle_CZ550","hlc_rifle_rpk74n","hlc_rifle_aks74u","CUP_glaunch_M79","rhs_weap_pkm","CUP_arifle_Sa58P","CUP_arifle_Sa58V","CUP_srifle_SVD","RHS_Weap_SVDP_WD","CUP_srifle_SVD_des","rhs_weap_rpg7","CUP_launch_RPG18","CUP_launch_9K32Strela","RH_ttracker","RH_tt33","RH_vz61","RH_mak","RH_cz75"];

[arsenal1, _asczArsenalItems1, true, true] call BIS_fnc_addVirtualItemCargo;
[arsenal1, _asczArsenalItems2,true, true] call BIS_fnc_addVirtualMagazineCargo;
[arsenal1, _asczArsenalItems3,true, true] call BIS_fnc_addVirtualBackpackCargo;
[arsenal1, _asczArsenalItems4, true, true] call BIS_fnc_addVirtualWeaponCargo;

[arsenal2, _asczArsenalItems1, true, true] call BIS_fnc_addVirtualItemCargo;
[arsenal2, _asczArsenalItems2,true, true] call BIS_fnc_addVirtualMagazineCargo;
[arsenal2, _asczArsenalItems3,true, true] call BIS_fnc_addVirtualBackpackCargo;
[arsenal2, _asczArsenalItems4, true, true] call BIS_fnc_addVirtualWeaponCargo;

[arsenal3, _asczArsenalItems1, true, true] call BIS_fnc_addVirtualItemCargo;
[arsenal3, _asczArsenalItems2,true, true] call BIS_fnc_addVirtualMagazineCargo;
[arsenal3, _asczArsenalItems3,true, true] call BIS_fnc_addVirtualBackpackCargo;
[arsenal3, _asczArsenalItems4, true, true] call BIS_fnc_addVirtualWeaponCargo;
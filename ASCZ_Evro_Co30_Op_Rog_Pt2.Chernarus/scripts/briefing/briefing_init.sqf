// ====================================================================================

// NOTES: SITUATION
// The code below creates the situation sub-section of notes.
// <br></br> je nový řádek

_sit = player createDiaryRecord ["diary", ["Situace","

Úspěšně jsme zatlačili ChDKZ na ústup a naše jednotky obsadili velkou část území ChDKZ, dokonce i CDF zatlačilo větší část jednotek nepřítele. Naše jednotky NAPA nyní drží vetší pozice na severo-východu a jiho-východu.
Naše jednotky nyní podporuje i CDF, situace vypadá nadějně a vítěztví je na dosah.

"]];

// ====================================================================================

// NOTES: MISSION
// The code below creates the mission sub-section of notes.

_mis = player createDiaryRecord ["diary", ["Mise","

Naším první úkolem je vytlačit nepřítele z Nového Soboru a postupně zatlačit nepřítele přes Kabanino až na hlavní letiště. Po úspěšném zajištění Kabanina můžeme počítat s pomocí CDF, kteří nám pomůžou s útokem na letiště.
<br></br>
<img image='img\pic-1.jpg' width='320'></img><br></br><br></br>

Na mapě je označeno několik přátelských pozic NAPA a CDF, pokud se něco posere nebo se někdo stratí tak by měl vyhledat tyto pozice.
<br></br>
<img image='img\pic-2.jpg' width='320'></img>

"]];
0 fadeSound 0;
0 fadeMusic 0;
cutText ["", "BLACK FADED", 99];

waitUntil {!(isNull player)};
sleep 0.5;

private "_color";
_color = resistance call BIS_fnc_sideColor; // dalsi barvy WEST, EAST
_color set [3, 0.33];
[
    getMarkerPos "hq_1", // nazev markeru
    "Co28 - Operace Rog Pt.2", // nazev mise
    150,
    200,
    0,
    1,
    [
        ["\a3\ui_f\data\map\markers\NATO\n_inf.paa", _color, markerPos "hq_1_1", 1, 1, 0, "3. Družstvo", 0],
        ["\a3\ui_f\data\map\markers\NATO\n_inf.paa", _color, markerPos "hq_1_1_1_1", 1, 1, 0, "1. Družstvo", 0],
        ["\a3\ui_f\data\map\markers\NATO\n_inf.paa", _color, markerPos "hq_1_1_1", 1, 1, 0, "2. Družstvo", 0],
        ["\a3\ui_f\data\map\markers\NATO\n_hq.paa", _color, markerPos "hq_1", 1, 1, 0, "HQ", 0]
    ]
] spawn BIS_fnc_establishingShot;

5 fadeSound 1;
5 fadeMusic 0.5;
cutText ["", "BLACK IN", 5];
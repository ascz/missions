if (!isDedicated && (isNull player)) then
{
    waitUntil {sleep 0.1; !isNull player};
};


// ====================================================================================

// NOTES: SITUATION
// The code below creates the situation sub-section of notes.
// <br></br> je nový řádek

_sit = player createDiaryRecord ["diary", ["Situace","

    Krátký popis 1

"]];

// ====================================================================================

// NOTES: MISSION
// The code below creates the mission sub-section of notes.

_mis = player createDiaryRecord ["diary", ["Mise","

    Krátký popis 1
    <br></br>
    <img image='img\pic-1.jpg' width='320'></img><br></br><br></br>

    Krátký popis 2
    <br></br>
    <img image='img\pic-2.jpg' width='320'></img><br></br><br></br>

    Krátký popis 3.

"]];
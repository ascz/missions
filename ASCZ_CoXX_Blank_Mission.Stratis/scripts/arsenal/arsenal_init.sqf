arsenal_faction	= paramsArray select 2;

// Globální itemy pro všechny frakce
_generalItems = ["Binocular","ItemCompass","ItemGPS","ItemMap","Toolkit","ItemWatch"];
_aceItems = ["ACE_CableTie","ACE_Banana","ACE_DAGR","ACE_Clacker","ACE_M26_Clacker","ACE_DefusalKit","ACE_DeadManSwitch","ACE_Cellphone","ACE_EarPlugs","ACE_HuntIR_monitor","ACE_Kestrel4500","ACE_UAVBattery","ACE_wirecutter","ACE_MapTools","ACE_atropine","ACE_fieldDressing","ACE_elasticBandage","ACE_quikclot","ACE_bloodIV","ACE_bloodIV_500","ACE_bloodIV_250","ACE_bodyBag","ACE_bodyBagObject","ACE_epinephrine","ACE_morphine","ACE_packingBandage","ACE_personalAidKit","ACE_plasmaIV","ACE_plasmaIV_500","ACE_plasmaIV_250","ACE_salineIV","ACE_salineIV_500","ACE_salineIV_250","ACE_surgicalKit","ACE_tourniquet","ACE_microDAGR","ACE_RangeTable_82mm","ACE_SpareBarrel","ACE_RangeCard","ACE_Sandbag_empty","ACE_Tripod","ACE_ATragMX","ACE_IR_Strobe_Item"];

// ARSENAL - VŠECHNO
//-------------------------------------------------------------
if (arsenal_faction == 1) then {
    // itemy
    _asczArsenalItems1 = true;

    // zasobniky
    _asczArsenalItems2 = true;

    // batohy
    _asczArsenalItems3 = true;

    // zbrane
    _asczArsenalItems4 = true;

    [arsenal1, _asczArsenalItems1, true, true] call BIS_fnc_addVirtualItemCargo;
    [arsenal1, _asczArsenalItems2,true, true] call BIS_fnc_addVirtualMagazineCargo;
    [arsenal1, _asczArsenalItems3,true, true] call BIS_fnc_addVirtualBackpackCargo;
    [arsenal1, _asczArsenalItems4, true, true] call BIS_fnc_addVirtualWeaponCargo;

    [arsenal2, _asczArsenalItems1, true, true] call BIS_fnc_addVirtualItemCargo;
    [arsenal2, _asczArsenalItems2,true, true] call BIS_fnc_addVirtualMagazineCargo;
    [arsenal2, _asczArsenalItems3,true, true] call BIS_fnc_addVirtualBackpackCargo;
    [arsenal2, _asczArsenalItems4, true, true] call BIS_fnc_addVirtualWeaponCargo;

    [arsenal3, _asczArsenalItems1, true, true] call BIS_fnc_addVirtualItemCargo;
    [arsenal3, _asczArsenalItems2,true, true] call BIS_fnc_addVirtualMagazineCargo;
    [arsenal3, _asczArsenalItems3,true, true] call BIS_fnc_addVirtualBackpackCargo;
    [arsenal3, _asczArsenalItems4, true, true] call BIS_fnc_addVirtualWeaponCargo;
};

// ARSENAL - NAPA
//-------------------------------------------------------------
if (arsenal_faction == 2) then {
    // itemy
    _googlesItems = ["G_Bandanna_tan","G_Bandanna_khk","G_Bandanna_oli","rhs_balaclava","G_Balaclava_blk","G_Balaclava_oli","G_Spectacles","G_Aviator","CUP_FR_NeckScarf","CUP_FR_NeckScarf2","kae_OldGlasses","kae_RoundGlasses","rhs_scarf","kae_SunGlasses"];
    _headgearItems = ["T_HoodTan","H_Bandanna_camo","H_Bandanna_khk","H_Bandanna_cbr","H_Bandanna_sgg","H_Cap_grn","CUP_H_FR_BandanaGreen","H_Bandana_brown","H_Bandana_khaki","CUP_H_FR_BandanaWdl","rhs_beanie_green","H_Cowboy_Hat_brown","H_Booniehat_khk","H_Booniehat_oli","H_Booniehat_tan","H_ShemagOpen_tan","H_Shemag_olive","rds_Woodlander_cap2","rds_Woodlander_cap4","rds_Villager_cap2","rds_Villager_cap4","rds_Villager_cap1","rds_worker_cap4"];
    _uniformItems = ["U_I_FullGhillie_lsh","U_I_FullGhillie_sard","U_CDF_A3_SniperGhillie","CUP_U_I_Napa_Flecktarn2","CUP_U_I_Napa_Flecktarn"];
    _vestItems = ["CUP_V_I_Carrier_Belt","V_TacVest_oli","V_I_G_resistanceLeader_F","CUP_V_B_RRV_TL"];
    _radioItems = ["ACRE_PRC343","ACRE_PRC77"];
    _weaponItems = ["CUP_optic_PSO_1","RHS_ACC_PSO1M2","CUP_SVD_camo_g"];
    _asczArsenalItems1 = _generalItems + _aceItems + _googlesItems + _headgearItems + _uniformItems + _vestItems + _radioItems + _weaponItems;

    // zasobniky
    _asczArsenalItems2 = true;

    // batohy
    _asczArsenalItems3 = ["CUP_B_AlicePack_Khaki","CUP_B_CivPack_WDL","B_FieldPack_oli","rhs_rpg_empty","rhs_sidor","B_TacticalPack_oli","B_Carryall_oli","rhs_assault_umbts","CUP_B_USMC_AssaultPack","B_CDF_A3_Backpack"];

    // zbrane
    _weaponsPrimary = ["rhs_weap_akm","rhs_weap_akm_gp25","rhs_weap_akms","rhs_weap_akms_gp25","CUP_srifle_CZ550","hlc_rifle_rpk74n","hlc_rifle_aks74u","CUP_glaunch_M79","rhs_weap_pkm","CUP_arifle_Sa58P","CUP_arifle_Sa58V","CUP_srifle_SVD","RHS_Weap_SVDP_WD","CUP_srifle_SVD_des"];
    _weaponsSecondary = ["rhs_weap_rpg7","CUP_launch_RPG18","CUP_launch_9K32Strela"];
    _weaponsLauncher = ["RH_ttracker","RH_tt33","RH_vz61","RH_mak","RH_cz75"];
    _asczArsenalItems4 = _weaponsPrimary + _weaponsSecondary + _weaponsLauncher;


    [arsenal1, _asczArsenalItems1, true, true] call BIS_fnc_addVirtualItemCargo;
    [arsenal1, _asczArsenalItems2,true, true] call BIS_fnc_addVirtualMagazineCargo;
    [arsenal1, _asczArsenalItems3,true, true] call BIS_fnc_addVirtualBackpackCargo;
    [arsenal1, _asczArsenalItems4, true, true] call BIS_fnc_addVirtualWeaponCargo;

    [arsenal2, _asczArsenalItems1, true, true] call BIS_fnc_addVirtualItemCargo;
    [arsenal2, _asczArsenalItems2,true, true] call BIS_fnc_addVirtualMagazineCargo;
    [arsenal2, _asczArsenalItems3,true, true] call BIS_fnc_addVirtualBackpackCargo;
    [arsenal2, _asczArsenalItems4, true, true] call BIS_fnc_addVirtualWeaponCargo;

    [arsenal3, _asczArsenalItems1, true, true] call BIS_fnc_addVirtualItemCargo;
    [arsenal3, _asczArsenalItems2,true, true] call BIS_fnc_addVirtualMagazineCargo;
    [arsenal3, _asczArsenalItems3,true, true] call BIS_fnc_addVirtualBackpackCargo;
    [arsenal3, _asczArsenalItems4, true, true] call BIS_fnc_addVirtualWeaponCargo;
};

// ARSENAL - RUSKO
//-------------------------------------------------------------
if (arsenal_faction == 3) then {
    // itemy
    _googlesItems = ["G_Balaclava_oli","G_Balaclava_blk","rhs_balaclava"];
    _headgearItems = ["H_Cowboy_Hat_brown"];
    _uniformItems = ["U_gorka2_e","rhs_uniform_flora_patchless_alt","LOP_U_UA_Fatigue_02","rhs_Booniehat_flora","rhs_6b28_green_ess","LOP_V_6B23_Medic_OLV","rhs_6b26_ess","rhs_pdu4","U_gorka7_e","rhs_6b28_green","rhs_6b27m_green","LOP_U_US_Fatigue_04","rhs_6b13_Flora_6sh92_vog","U_gorka3_e","rhs_acc_dtk","rhs_acc_1p78","CUP_optic_PSO_1","CUP_H_FR_BandanaGreen","rhs_acc_pkas","rhs_fieldcap","rhs_acc_tgpa","U_gorka_e","rhs_beanie_green"];
    _vestItems = ["rhs_vydra_3m","rhs_6b13_Flora_6sh92","rhs_6sh92","LOP_V_Chestrig_Kamysh","rhs_6b23_rifleman"];
    _radioItems = ["ACRE_PRC343","ACRE_PRC77","ACRE_PRC148"];
    _weaponItems = ["rhs_acc_pso1m2"];
    _asczArsenalItems1 = _generalItems + _aceItems + _googlesItems + _headgearItems + _uniformItems + _vestItems + _radioItems + _weaponItems;

    // zasobniky
    _asczArsenalItems2 = true;

    // batohy
    _asczArsenalItems3 = ["B_Carryall_oli","CUP_B_CivPack_WDL","rhs_assault_umbts","rhs_rpg_empty","rhs_sidor"];

    // zbrane
    _weaponsPrimary = ["rhs_weap_akm","rhs_weap_akm_gp25","hlc_rifle_rpk74n","rhs_weap_ak74m_plummag","rhs_weap_akms","hlc_rifle_aks74u","CUP_smg_bizon","rhs_weap_svdp","rhs_weap_ak103_1","rhs_weap_ak74m","rhs_weap_ak103_1","rhs_weap_ak74m_gp25","CUP_srifle_VSSVintorez","rhs_weap_akms_gp25","hlc_rifle_rpk74n","hlc_rifle_aks74u","rhs_weap_pkm","rhs_weap_ak74m_camo"];
    _weaponsSecondary = ["rhs_weap_makarov_pmm","rhs_weap_pya"];
    _weaponsLauncher = ["rhs_weap_rpg26","rhs_weap_rpg7","CUP_launch_RPG18"];
    _asczArsenalItems4 = _weaponsPrimary + _weaponsSecondary + _weaponsLauncher;

    [arsenal1, _asczArsenalItems1, true, true] call BIS_fnc_addVirtualItemCargo;
    [arsenal1, _asczArsenalItems2,true, true] call BIS_fnc_addVirtualMagazineCargo;
    [arsenal1, _asczArsenalItems3,true, true] call BIS_fnc_addVirtualBackpackCargo;
    [arsenal1, _asczArsenalItems4, true, true] call BIS_fnc_addVirtualWeaponCargo;

    [arsenal2, _asczArsenalItems1, true, true] call BIS_fnc_addVirtualItemCargo;
    [arsenal2, _asczArsenalItems2,true, true] call BIS_fnc_addVirtualMagazineCargo;
    [arsenal2, _asczArsenalItems3,true, true] call BIS_fnc_addVirtualBackpackCargo;
    [arsenal2, _asczArsenalItems4, true, true] call BIS_fnc_addVirtualWeaponCargo;

    [arsenal3, _asczArsenalItems1, true, true] call BIS_fnc_addVirtualItemCargo;
    [arsenal3, _asczArsenalItems2,true, true] call BIS_fnc_addVirtualMagazineCargo;
    [arsenal3, _asczArsenalItems3,true, true] call BIS_fnc_addVirtualBackpackCargo;
    [arsenal3, _asczArsenalItems4, true, true] call BIS_fnc_addVirtualWeaponCargo;
};

// ARSENAL - DALŠÍ FRAKCE **VYPLNIT**
//-------------------------------------------------------------
if (arsenal_faction == 4) then {
    // itemy
    _googlesItems = [];
    _headgearItems = [];
    _uniformItems = [];
    _vestItems = [];
    _radioItems = ["ACRE_PRC343","ACRE_PRC77","ACR_PRC148","ACR_PRC152","ACR_PRC117"];
    _weaponItems = [];
    _asczArsenalItems1 = _generalItems + _aceItems + _googlesItems + _headgearItems + _uniformItems + _vestItems + _radioItems + _weaponItems;

    // zasobniky
    _asczArsenalItems2 = true;

    // batohy
    _asczArsenalItems3 = ["B_FieldPack_oli"];

    // zbrane
    _weaponsPrimary = [];
    _weaponsSecondary = [];
    _weaponsLauncher = [];
    _asczArsenalItems4 = _weaponsPrimary + _weaponsSecondary + _weaponsLauncher;

    [arsenal1, _asczArsenalItems1, true, true] call BIS_fnc_addVirtualItemCargo;
    [arsenal1, _asczArsenalItems2,true, true] call BIS_fnc_addVirtualMagazineCargo;
    [arsenal1, _asczArsenalItems3,true, true] call BIS_fnc_addVirtualBackpackCargo;
    [arsenal1, _asczArsenalItems4, true, true] call BIS_fnc_addVirtualWeaponCargo;

    [arsenal2, _asczArsenalItems1, true, true] call BIS_fnc_addVirtualItemCargo;
    [arsenal2, _asczArsenalItems2,true, true] call BIS_fnc_addVirtualMagazineCargo;
    [arsenal2, _asczArsenalItems3,true, true] call BIS_fnc_addVirtualBackpackCargo;
    [arsenal2, _asczArsenalItems4, true, true] call BIS_fnc_addVirtualWeaponCargo;

    [arsenal3, _asczArsenalItems1, true, true] call BIS_fnc_addVirtualItemCargo;
    [arsenal3, _asczArsenalItems2,true, true] call BIS_fnc_addVirtualMagazineCargo;
    [arsenal3, _asczArsenalItems3,true, true] call BIS_fnc_addVirtualBackpackCargo;
    [arsenal3, _asczArsenalItems4, true, true] call BIS_fnc_addVirtualWeaponCargo;
};
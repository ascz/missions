/*
 INS_AI_Halo.sqf by Jigsor
 Runs only if caller is group leader and has AI in his squad.
 Script will open map and wait for map click of location for AI to halo from then call function INS_AI_Halo to place existing backpack if any in ventral position and add parachute backpack.
 Once landed, parachute and parachute backpack is deleted and original backpack is returned.
 Uses ATM's reliable getloadout and setloadout functions.
*/

if(not local player) exitWith{};
if (leader group player != player) exitWith {
	player sideChat "STR_BMR_recruit_restrict_halo";
};

if (count units group player < 2) exitWith {};

private ["_aiArr","_halopos"];
_aiArr = [];
_grp = group player;

{if !(isPlayer _x) then {_aiArr pushBack _x;};} forEach (units _grp);

mapclick = false;
openMap true;

waitUntil {visibleMap};
[] spawn {[localize "STR_BMR_ai_halo_mapclick",0,.1,3,.005,.1] call bis_fnc_dynamictext;};

["AIdrop_mapclick","onMapSingleClick", {
	clickpos = _pos;
	mapclick = true;
}] call BIS_fnc_addStackedEventHandler;

waitUntil {mapclick or !(visibleMap)};
["AIdrop_mapclick", "onMapSingleClick"] call BIS_fnc_removeStackedEventHandler;

if (!visibleMap) exitwith {};
if (mapclick) then {hint localize "STR_BMR_ai_halo_inprogress"};
sleep 1;
openMap false;

_halopos = clickpos;
{
	[_x,_halopos] spawn INS_AI_Halo;
	sleep 2.5;
} count _aiArr;
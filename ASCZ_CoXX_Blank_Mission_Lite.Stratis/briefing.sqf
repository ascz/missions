if (!isDedicated && (isNull player)) then
{
    waitUntil {sleep 0.1; !isNull player};
};


// ====================================================================================

// NOTES: SITUATION
// The code below creates the situation sub-section of notes.
// <br></br> je nový řádek
// <img image='img\pic-1.jpg' width='320'></img> vlozeni obrazku

// SITUACE
_sit = player createDiaryRecord ["diary", ["Situace","

    Krátký popis 1

"]];

// ====================================================================================

// NOTES: MISSION
// The code below creates the mission sub-section of notes.

// POSTUP MISE
_mis = player createDiaryRecord ["diary", ["Mise","

    Krátký popis 1

"]];
class Params
{
	class Daytime
	{
		title = $STR_a3_cfgvehicles_moduledate_f_arguments_hour_0;
		values[] = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23};
		texts[] = {"00:00","01:00","02:00","03:00","04:00","05:00","06:00","07:00","08:00","09:00","10:00","11:00","12:00","13:00","14:00","15:00","16:00","17:00","18:00","19:00","20:00","21:00","22:00","23:00"};
		default = 9;
		function = "BIS_fnc_paramDaytime";
	};
	class Weather
	{
		title = $STR_A3_rscattributeovercast_title;
		values[] = {0,25,50,75,100};
		texts[] = {
			$STR_A3_rscattributeovercast_value000_tooltip,
			$STR_A3_rscattributeovercast_value025_tooltip,
			$STR_A3_rscattributeovercast_value050_tooltip,
			$STR_A3_rscattributeovercast_value075_tooltip,
			$STR_A3_rscattributeovercast_value100_tooltip
		};
		default = 50;
		function = "BIS_fnc_paramWeather";
	};
    class ASCZ_Arsenal
    {
        title = "Arsenal";
        values[] = {1,2,3,4};
        texts[] = {
            "Všechno",
            "NAPA",
            "Rusko",
            "**Nová frakce**"
        };
        default = 1;
    };
};

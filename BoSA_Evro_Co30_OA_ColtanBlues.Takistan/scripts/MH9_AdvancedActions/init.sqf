/*###############################/
	USAGE and SETTINGS:
	Open and edit the file "settings.sqf"
	For add or modify an language, open & modify the file "language.sqf" (French & English Language currently)
	
/*###############################/
	/!\ SCRIPT	DON'T TOUCH /!\	 
/###############################*/
waitUntil {time > 1};
_vehicle = _this select 0; 
if (typeOf _vehicle != "B_Heli_Light_01_F") exitWith {};

// Define path + file script
#define MH9AA_PATH_SCRIPT "scripts\MH9_AdvancedActions\" 		//Directory path for the scripts (default: "scripts\MH9_AdvancedAction\")
#define MH9AA_PATH_settings MH9AA_PATH_SCRIPT+"settings.sqf"	// The filename for the settings (default: settings.sqf)
#define MH9AA_PATH_language MH9AA_PATH_SCRIPT+"language.sqf"	// The filename for the languages (default: language.sqf)
#define MH9AA_PATH_fnc MH9AA_PATH_SCRIPT+"fnc.sqf"				// The filename for the functions (default: fnc.sqf)
#define MH9AA_PATH_getin MH9AA_PATH_SCRIPT+"getin.sqf"			// The filename for get in action (default: getin.sqf)
#define MH9AA_PATH_bench MH9AA_PATH_SCRIPT+"bench.sqf"			// The filename for get in action (default: bench.sqf)

// Load Settings && Language
_loadsettings = [] execVM MH9AA_PATH_settings;
_loadfnc = [] execVM MH9AA_PATH_fnc;	 
_loadlanguage = [] execVM MH9AA_PATH_language;
waitUntil{scriptDone _loadsettings && scriptDone _loadlanguage && scriptDone _loadfnc};

_MH9AA_LockBench = if(count _this > 1) then { _this select 1 };
_MH9AA_LockBench = if ( isNil "_MH9AA_LockBench" ) then { MH9AA_LockBench } else { _this select 1 };
_MH9AA_SpawnBenchUP = if(count _this > 2) then { _this select 2 };
_MH9AA_SpawnBenchUP = if ( isNil "_MH9AA_SpawnBenchUP" ) then { MH9AA_SpawnBenchUP } else { _this select 2 };
_MH9AA_GetOnBenchAction = if(count _this > 3) then { _this select 3 };
_MH9AA_GetOnBenchAction = if ( isNil "_MH9AA_GetOnBenchAction" ) then { MH9AA_GetOnBenchAction } else { _this select 3 };
_MH9AA_PilotCommandBench = if(count _this > 4) then { _this select 4 };
_MH9AA_PilotCommandBench = if ( isNil "_MH9AA_PilotCommandBench" ) then { MH9AA_PilotCommandBench } else { _this select 4 };
_MH9AA_RemoveBench = if(count _this > 5) then { _this select 5 };
_MH9AA_RemoveBench = if ( isNil "_MH9AA_RemoveBench" ) then { MH9AA_RemoveBench } else { _this select 5 };
_MH9AA_AddDoors = if(count _this > 6) then { _this select 6 };
_MH9AA_AddDoors = if ( isNil "_MH9AA_AddDoors" ) then { MH9AA_AddDoors } else { _this select 6 };
_MH9AA_AddFlir = if(count _this > 7) then { _this select 7 };
_MH9AA_AddFlir = if ( isNil "_MH9AA_AddFlir" ) then { MH9AA_AddFlir } else { _this select 7 };
_MH9AA_RemoveTread = if(count _this > 8) then { _this select 8 };
_MH9AA_RemoveTread = if ( isNil "_MH9AA_RemoveTread" ) then { MH9AA_RemoveTread } else { _this select 8 };
_MH9AA_ChangeTexture = if(count _this > 9) then { _this select 9 };
_MH9AA_ChangeTexture = if ( isNil "_MH9AA_ChangeTexture" ) then { MH9AA_ChangeTexture } else { _this select 9 };


// Script Enable ?
if (isNil "MH9AA_EnableScript" or !MH9AA_EnableScript) exitWith {};

if (isServer) then 
{
	// Spawn with Bench UP
	if (_MH9AA_SpawnBenchUP) then 
	{
		_vehicle animate ["BenchL_Up",1];
		_vehicle animate ["BenchR_Up",1];
	};
	// Remove Bench
	if (_MH9AA_RemoveBench or _MH9AA_AddDoors or _MH9AA_AddFlir) then
	{
		_vehicle animate ["AddBenches",0];
	};
	// Add doors
	if (_MH9AA_AddDoors) then
	{
		_vehicle animate ["AddDoors",1];
	};
	// Add FLIR + Support
	if (_MH9AA_AddFlir) then
	{
		_vehicle animate ["AddFLIR",1];
		_vehicle animate ["AddHoldingFrame",1];
		_vehicle animate ["AddScreen1",1];
		_vehicle animate ["AddTread_Short",1];
	};
	// Remove Tread
	if (_MH9AA_RemoveTread or _MH9AA_AddFlir) then
	{
		_vehicle animate ["AddTread",0];
	};
	// Lock Bench 
	if (_MH9AA_RemoveBench or _MH9AA_LockBench or _MH9AA_SpawnBenchUP or _MH9AA_AddDoors or _MH9AA_AddFlir) then
	{
		[_vehicle,true] call MH9AA_fnc_LockBench;
	};
};

if (isDedicated or !(isPlayer player)) exitWith {};
waitUntil {!isNull player};

if (_MH9AA_ChangeTexture != "") then
{
	private ["_texture"];
	_texture = nil;
	switch (_MH9AA_ChangeTexture) do 
	{
		case "blueline"  : { _texture = "a3\air_f\Heli_Light_01\Data\Skins\heli_light_01_ext_blueline_co.paa"; };
		case "digital"  : { _texture = "a3\air_f\Heli_Light_01\Data\Skins\heli_light_01_ext_digital_co.paa"; };
		case "elliptical"  : { _texture = "a3\air_f\Heli_Light_01\Data\Skins\heli_light_01_ext_elliptical_co.paa"; };
		case "furious"  : { _texture = "a3\air_f\Heli_Light_01\Data\Skins\heli_light_01_ext_furious_co.paa"; };
		case "graywatcher"  : { _texture = "a3\air_f\Heli_Light_01\Data\Skins\heli_light_01_ext_graywatcher_co.paa"; };
		case "jeans"  : { _texture = "a3\air_f\Heli_Light_01\Data\Skins\heli_light_01_ext_jeans_co.paa"; };
		case "shadow"  : { _texture = "a3\air_f\Heli_Light_01\Data\Skins\heli_light_01_ext_shadow_co.paa"; };
		case "sheriff"  : { _texture = "a3\air_f\Heli_Light_01\Data\Skins\heli_light_01_ext_sheriff_co.paa"; };
		case "speedy"  : { _texture = "a3\air_f\Heli_Light_01\Data\Skins\heli_light_01_ext_speedy_co.paa"; };
		case "sunset"  : { _texture = "a3\air_f\Heli_Light_01\Data\Skins\heli_light_01_ext_sunset_co.paa"; };
		case "vrana"  : { _texture = "a3\air_f\Heli_Light_01\Data\Skins\heli_light_01_ext_vrana_co.paa"; };
		case "wasp"  : { _texture = "a3\air_f\Heli_Light_01\Data\Skins\heli_light_01_ext_wasp_co.paa"; };
		case "wave"  : { _texture = "a3\air_f\Heli_Light_01\Data\Skins\heli_light_01_ext_wave_co.paa"; };
		case "blue"  : { _texture = "a3\air_f\Heli_Light_01\Data\heli_light_01_ext_blue_co.paa"; };
		case "co"  : { _texture = "a3\air_f\Heli_Light_01\Data\heli_light_01_ext_co.paa"; };
		case "indp"  : { _texture = "a3\air_f\Heli_Light_01\Data\heli_light_01_ext_indp_co.paa"; };
		case "ion"  : { _texture = "a3\air_f\Heli_Light_01\Data\heli_light_01_ext_ion_co.paa"; };
		default { _texture = format ["#(argb,8,8,3)color(%1)",_MH9AA_ChangeTexture]; } ;
	};
	if (!isNil "_texture") then
	{
		_vehicle setObjectTexture [0,_texture];
	};	
};

if (_MH9AA_RemoveBench or _MH9AA_LockBench or _MH9AA_AddDoors or _MH9AA_AddFlir) exitWith {};
// Wait if the vehicle is locked ...
waitUntil {sleep 0.2; locked _vehicle <= 1};

if (_MH9AA_GetOnBenchAction && !(_MH9AA_AddDoors)) then 
{
	// Add Action Right && Left
	_id = _vehicle addAction [MH9AA_STR_GETIN_BENCH_R, MH9AA_PATH_getin, ["R"], 5.39, false, true, "", 
			"
				if (_target distance player < 4.5 && _target call MH9AA_fnc_LockedBench && _target emptyPositions 'Cargo' > 0) then
				{
					if (count ([_target,'R'] call MH9AA_fnc_EmptyPositionsBench) > 0) then 
					{
						[player,_target,'R'] call MH9AA_fnc_ActionBench
					}
				}
			"];
	_id = _vehicle addAction [MH9AA_STR_GETIN_BENCH_L, MH9AA_PATH_getin, ["L"], 5.39, false, true, "", 
			"
				if (_target distance player < 4.5 && _target call MH9AA_fnc_LockedBench && _target emptyPositions 'Cargo' > 0 ) then
				{
					if (count ([_target,'L'] call MH9AA_fnc_EmptyPositionsBench) > 0) then 
					{
						[player,_target,'L'] call MH9AA_fnc_ActionBench
					}
				}
			"];
};

if (_MH9AA_PilotCommandBench) then 
{

	_vehicle addEventHandler ["GetIn",
		{
			_vehicle = _this select 0;
			_position = _this select 1;
			_unit = _this select 2;

			if (_unit == player) then 
			{
				if (_position == "driver") then 
				{
					private ["_bench_status","_id"];

					_bench_status = _vehicle animationPhase "BenchL_Up";
					
					if (_bench_status == 1) then
					{
						_id = _unit addAction [MH9AA_STR_BENCH_DOWN, MH9AA_PATH_bench, [_vehicle,"DOWN",MH9AA_PATH_bench], 5, false, true, "", "count ([vehicle player] call MH9AA_fnc_EmptyPositionsBench) == 4"];
					};
					if (_bench_status == 0 or isNil "_id") then
					{
						_vehicle animate ["BenchL_Up",0];
						_vehicle animate ["BenchR_Up",0];

						_id = _unit addAction [MH9AA_STR_BENCH_UP, MH9AA_PATH_bench, [_vehicle,"UP",MH9AA_PATH_bench], 5, false, true, "", " count ([vehicle player] call MH9AA_fnc_EmptyPositionsBench) == 4"];
					};

					_unit setvariable ["MH9AA_Bench_IdAction", _id];

				};
			};
		}];

	_vehicle addEventHandler ["GetOut",
		{
			_vehicle = _this select 0;
			_position = _this select 1; //"driver", "gunner", "commander" or "cargo"
			_unit = _this select 2;

			if (_unit == player) then 
			{
				if (_position == "driver") then 
				{
					_IdAction = _unit getVariable "MH9AA_Bench_IdAction";
					if (!isNil "_IdAction") then
					{
						_unit removeAction _IdAction;
					};
				};
			};
		}];

};










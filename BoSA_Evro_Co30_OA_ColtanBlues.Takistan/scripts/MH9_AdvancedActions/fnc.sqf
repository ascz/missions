/*###############################/
	USAGE and SETTINGS:
	Open and edit the file "settings.sqf"
	For add or modify an language, open & modify the file "language.sqf" (French & English Language currently)
	
/*###############################/
	/!\ SCRIPT	DON'T TOUCH /!\	 
/###############################*/

MH9AA_fnc_ActionBench = {
	private ["_vehicle","_caller","_sidebench","_return"];
	
	_caller = _this select 0;
	_vehicle = _this select 1;
	_sidebench = _this select 2;

	_return = false;
	
	if(vehicle _caller == _caller) then
	{
		private["_posWTM"];
		
		_posWTM = _vehicle worldToModel (getposATL _caller);

		if (_sidebench == "R") then
		{ 
			if ((_posWTM select 0) + 0.71 > 0 ) then
			{
				if (_posWTM distance [1,0.52,-0.5] < 3) then
				{
					_return = true;
				};
			};
		};
		if (_sidebench == "L") then
		{		
			if ((_posWTM select 0) + 0.94 < 0 ) then
			{
				if (_posWTM distance [-1,0.52,-0.5] < 3) then
				{
					_return = true;
				};
			};
		}; 
	};
	_return
};

MH9AA_fnc_LockedBench = {
	
	if (_this lockedCargo 2 && _this lockedCargo 3 && _this lockedCargo 4 && _this lockedCargo 5) then
	{
		false
	}
	else
	{
		true
	};
};

MH9AA_fnc_LockBench = {
	private ["_vehicle","_lock"];
	_vehicle = _this select 0;
	_lock = _this select 1;
	
	_vehicle lockCargo [2,_lock];
	_vehicle lockCargo [3,_lock];
	_vehicle lockCargo [4,_lock];
	_vehicle lockCargo [5,_lock];
};


MH9AA_fnc_EmptyPositionsBench = {
	private ["_vehicle","_sidebench","_ReturnNum"];
	_vehicle = _this select 0;
	_sidebench = _this select 1; //Optional
	_ReturnNum = nil;

	if(!(isNil "_vehicle") or typename _vehicle == "OBJECT") then
	{
		private ["_BenchSidePos","_BenchNum"];
		if (_sidebench == "R") then { _BenchSidePos = [[0.920,0.964,-0.451],[0.922,0.066,-0.457]]; _BenchNum = [2,5]; };
		if (_sidebench == "L") then { _BenchSidePos = [[-0.911,0.062,-0.454],[-0.945,0.968,-0.459]]; _BenchNum = [3,4]; };
		if (isNil "_sidebench") then {_BenchSidePos = [[0.920,0.964,-0.451],[-0.911,0.062,-0.454],[-0.945,0.968,-0.459],[0.922,0.066,-0.457]]; _BenchNum = [2,3,4,5]; };
		
		_ReturnNum = _BenchNum;
		
		if (_vehicle emptyPositions "Cargo" != 6) then
		{
			{
				if (alive _x) then 
				{
					_xWTM = _vehicle worldToModel (getposATL _x);
					{
						if ( _xWTM distance _x < 0.05 ) then
						{
							_ReturnNum = _ReturnNum - [_BenchNum select _forEachIndex];
						};
					}forEach _BenchSidePos;
				};
			} forEach crew _vehicle;
		};
	};

	_ReturnNum
};




/*###############################/
	USAGE and SETTINGS:
	Open and edit the file "settings.sqf"
	You can here, add or modify an language (French, Italian & English Language currently)
	
/*###############################/
			LANGUAGE	 
/###############################*/

switch (language) do 
{
	case "Czech":
	{
		MH9AA_STR_GETIN_BENCH_R		= "Nastup do MH-6 Little Bird - Lavice v pravo";
		MH9AA_STR_GETIN_BENCH_L		= "Nastup do MH-6 Little Bird - Lavice v  levo";
		MH9AA_STR_BENCH_UP			= "Zvednout lavice";
		MH9AA_STR_BENCH_DOWN		= "Snížit lavice";
	};
	case "French":
	{
		MH9AA_STR_GETIN_BENCH_R		= "Grimper dans MH-9 Hummingbird - Nacelle droite";
		MH9AA_STR_GETIN_BENCH_L		= "Grimper dans MH-9 Hummingbird - Nacelle gauche";
		MH9AA_STR_BENCH_UP			= "Relever les nacelles";
		MH9AA_STR_BENCH_DOWN		= "Abaisser les nacelles";
	};
//	case "German":
//	{
//		MH9AA_STR_GETIN_BENCH_R		= "Get in MH-9 Hummingbird - Bench right";
//		MH9AA_STR_GETIN_BENCH_L		= "Get in MH-9 Hummingbird - Bench left";
//		MH9AA_STR_BENCH_UP			= "Up bench";
//		MH9AA_STR_BENCH_DOWN		= "Down bench";
//	};
//	case "Polish":
//	{
//		MH9AA_STR_GETIN_BENCH_R		= "Get in MH-9 Hummingbird - Bench right";
//		MH9AA_STR_GETIN_BENCH_L		= "Get in MH-9 Hummingbird - Bench left";
//		MH9AA_STR_BENCH_UP			= "Up bench";
//		MH9AA_STR_BENCH_DOWN		= "Down bench";
//	};
//	case "Portuguese":
//	{
//		MH9AA_STR_GETIN_BENCH_R		= "Get in MH-9 Hummingbird - Bench right";
//		MH9AA_STR_GETIN_BENCH_L		= "Get in MH-9 Hummingbird - Bench left";
//		MH9AA_STR_BENCH_UP			= "Up bench";
//		MH9AA_STR_BENCH_DOWN		= "Down bench";
//	};
//	case "Russian":
//	{
//		MH9AA_STR_GETIN_BENCH_R		= "Get in MH-9 Hummingbird - Bench right";
//		MH9AA_STR_GETIN_BENCH_L		= "Get in MH-9 Hummingbird - Bench left";
//		MH9AA_STR_BENCH_UP			= "Up bench";
//		MH9AA_STR_BENCH_DOWN		= "Down bench";
//	};
//	case "Spanish":
//	{
//		MH9AA_STR_GETIN_BENCH_R		= "Get in MH-9 Hummingbird - Bench right";
//		MH9AA_STR_GETIN_BENCH_L		= "Get in MH-9 Hummingbird - Bench left";
//		MH9AA_STR_BENCH_UP			= "Up bench";
//		MH9AA_STR_BENCH_DOWN		= "Down bench";
//	};
	default
	{
		MH9AA_STR_GETIN_BENCH_R		= "Get in MH-9 Hummingbird - Bench right";
		MH9AA_STR_GETIN_BENCH_L		= "Get in MH-9 Hummingbird - Bench left";
		MH9AA_STR_BENCH_UP			= "Up bench";
		MH9AA_STR_BENCH_DOWN		= "Down bench";
	}
};


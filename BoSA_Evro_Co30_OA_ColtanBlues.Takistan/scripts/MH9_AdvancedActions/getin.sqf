/*###############################/
	USAGE and SETTINGS:
	Open and edit the file "settings.sqf"
	For add or modify an language, open & modify the file "language.sqf" (French & English Language currently)
	
/*###############################/
	/!\ SCRIPT	DON'T TOUCH /!\	 
/###############################*/

// settings
_target = _this select 0;
_caller = _this select 1;
_id = _this select 2;
_arg = _this select 3;
//
_sidebench = _arg select 0;

_BenchNum = [_target, _sidebench] call MH9AA_fnc_EmptyPositionsBench;

_BenchNum = if (count _BenchNum > 1) then { _BenchNum call BIS_fnc_selectRandom } else { _BenchNum select 0 };

_caller action ["GetInCargo", _target, _BenchNum];


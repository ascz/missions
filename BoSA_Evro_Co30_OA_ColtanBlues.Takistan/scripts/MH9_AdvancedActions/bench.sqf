/*###############################/
	USAGE and SETTINGS:
	Open and edit the file "settings.sqf"
	For add or modify an language, open & modify the file "language.sqf" (French & English Language currently)
	
/*###############################/
	/!\ SCRIPT	DON'T TOUCH /!\	 
/###############################*/

// settings
_target = _this select 0;
_caller = _this select 1;
_id = _this select 2;
_arg = _this select 3;
//
_vehicle = _arg select 0;
_move = _arg select 1;
_script = _arg select 2;

_caller removeAction _id;

_anim = 0;
_str_lang = MH9AA_STR_BENCH_UP;
_param = "UP";
_lock = false;

if (_move == "UP") then 
{
	_anim = 1;
	_str_lang = MH9AA_STR_BENCH_DOWN;
	_param = "DOWN";
	_lock = true;
};

[_vehicle,_lock] call MH9AA_fnc_LockBench;

_vehicle animate ["BenchL_Up",_anim];
_vehicle animate ["BenchR_Up",_anim];

_id = _caller addAction [_str_lang, _script, [_vehicle,_param,_script], 5, false, true, "", "count ([vehicle player] call MH9AA_fnc_EmptyPositionsBench) == 4"];
_caller setvariable ["MH9AA_Bench_IdAction", _id];
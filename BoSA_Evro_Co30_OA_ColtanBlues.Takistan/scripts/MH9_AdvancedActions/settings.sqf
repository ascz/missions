/*###############################/
	SCRIPT: MH9 Advanced Actions
	VERSION: 1.0
	AUTHOR: TittErS	
	
	At placed in Initialization of MH9
	
	Use general settings (settings.sqf)
		null = [this] execVM 'scripts\MH9_AdvancedActions\init.sqf';
	
	Custom setting with the parameters (use nil for used general settings (settings.sqf))
		null = [this,nil,true,nil,false] execVM 'scripts\MH9_AdvancedActions\init.sqf';
		
	Changed the texture in using the general settings (settings.sqf)
		null = [this,nil,nil,nil,nil,nil,nil,nil,nil,"digital"] execVM 'scripts\MH9_AdvancedActions\init.sqf';
	
	Params:
	1: Lock Bench (BOOLEAN) 
	2: Spawn Bench UP (BOOLEAN)
	3: GetOn Bench Action (BOOLEAN)
	4: Pilot Command Bench (BOOLEAN)
	5: Remove Bench (BOOLEAN)
	6: Add Doors (BOOLEAN)
	7: Add Flir (BOOLEAN)
	8: Remove Tread (BOOLEAN)
	9: Change Texture (STRING)
		

	SETTINGS:
		You can modify the options in this file.
		For add or modify an language, open and modify the file "language.sqf" (French & English Language currently)


/*###############################/
			SETTINGS	 
/###############################*/

MH9AA_EnableScript			= true;			// Enable or Disable Script. (DEFAULT: true)


MH9AA_LockBench 			= false;		// Lock or Unlock bench. (DEFAULT: false)
MH9AA_SpawnBenchUP			= false;		// MH9 spawn with the benchs up. (DEFAULT: false)
MH9AA_GetOnBenchAction 		= true;			// Add actions to left and right for get on bench. (DEFAULT: true)
MH9AA_PilotCommandBench 	= true;			// Add actions to pilot for controled the benchs (UP/DOWN). (DEFAULT: true)

MH9AA_RemoveBench 			= false;		// Remove the benchs. (DEFAULT: false)
MH9AA_AddDoors				= false;		// /!\VERSION DEV ONLY/!\ Add doors (DEFAULT: false)
MH9AA_AddFlir				= false;		// /!\VERSION DEV ONLY/!\ Add Flir,Support and LCD (DEFAULT: false)
MH9AA_RemoveTread			= false;		// Remove Tread (DEFAULT: false)
MH9AA_ChangeTexture			= "0.1,0.1,0.1,1";			// Changed the texture with RGBA color (between 0 and 1) or name texture (see the list texture) (RGBA example: "0.19,0.17,0.12,1") (DEFAULT: "")


/*---------------------------------------------------------------------------
LIST TEXTURES


"blueline"
"digital"
"elliptical"
"furious"
"graywatcher"
"jeans"
"shadow"
"sheriff"
"speedy"
"sunset"
"vrana"
"wasp"
"wave"
"blue"
"co"
"indp"
"ion"


----------------------------------------------------------------------------*/










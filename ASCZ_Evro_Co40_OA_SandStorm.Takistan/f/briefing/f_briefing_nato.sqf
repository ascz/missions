// NOTES: MISSION
// The code below creates the mission sub-section of notes.

_mis = player createDiaryRecord ["diary", ["Mise","
<br/>
1. a 2. Družstvo má za úkol obsadit města poblíž letiště Rasman a letiště samotné. V prvním městě se nachází možná opuštěná základna kde možná budou nějáké informace o Taksitánské armádě.<br></br><img image='hqtabule1.jpg' width='320'></img><br></br><br></br>3. Družstvo bude dopraveno Novembrem na severo-východ, odkud se vydá na jih směrem k letišti. Jsou zde nepřátelská AA postavení, které jsou potřeba zneškodnit. Jakmile 3. Družstvo tyto postavení vyčistí bude čekat na další rozkazy. Je možné že někde poblíž letiště se nachází důležité HVT osoby, které jsou potřeba zajmout nebo eliminovat.<br></br><img image='hqtabule2.jpg' width='320'></img><br></br><br></br>Po úspěšném dokončení svých úkolů se jednotky přeskupí a zajistí pozice na severu letiště, je možné že se nepřítel pokusí znovu obsadit letiště jednotkami z hlavního města Zargabád.
"]];

// ====================================================================================

// NOTES: SITUATION
// The code below creates the situation sub-section of notes.

_sit = player createDiaryRecord ["diary", ["Situace","
<br/>
Obrané linie u letiště Rasman byly proraženy a nepřítel se stáhnul na letiště a to je nyní těžce bráněno. Je potřeba zaútočit vším co máme! Začíná Operace Sand Storm.
<br/><br/>
PŘÁTELSKÉ JEDNOTKY
<br/>
Na oblast kolem letiště budou útočit čtyři družstva - dvě pěchotní družstva, družstvo speciálních jednotek a tankové družstvo Yankee. K dispozici je také letecká podpora s volacím znakem November.
"]];

// ====================================================================================
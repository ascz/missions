// NOTES: MISSION
// The code below creates the mission sub-section of notes.

_mis = player createDiaryRecord ["diary", ["Mise","
<br/>
Jednotky NAPA mají za úkol obsadit strategické body v Černarusi. Nepřítelem je ChDKZ a ten obsadil většinu severní Černarusi. Ve vesinici Staroje má ChDKZ větší postavení, je potřeba tuto oblast zajistit. <br></br><img image='img\pic-1.jpg' width='320'></img><br></br><br></br>Dále na sever od Staroje má ChDKZ v černém lese několik menších táborů, v jednom z nich se skrývá jejich velitel a toho musíme dostat živého či mrtvého (foto níže).<br></br><img image='img\pic-2.jpg' width='320'></img><br></br><br></br>Po úspěšném dokončení těchto úkolů vyrazíme osvobodit Nový Sobor a uděláme si z něho nový opěrný bod.
"]];

// ====================================================================================

// NOTES: SITUATION
// The code below creates the situation sub-section of notes.

_sit = player createDiaryRecord ["diary", ["Situace","
<br/>
Černarus je v občanské válce, většinu území nyní ovládá proruská ChDKZ. Černaruská armáda CDF byla zahnána na jiho-západní Černarus. Naše jednotky NAPA drží menší pozice na jiho-východu, jelikož snaha CDF je marná tak jsme se rozhodli vzít věci do svých rukou a zaútočit na ChDKZ na vlastní pěst.
"]];

// ====================================================================================
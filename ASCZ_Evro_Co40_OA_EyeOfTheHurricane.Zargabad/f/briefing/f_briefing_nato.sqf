// NOTES: MISSION
// The code below creates the mission sub-section of notes.

_mis = player createDiaryRecord ["diary", ["Mise","
<br/>
1. a 2. Družstvo má za úkol obsadit letiště v Zargabádu. Na letišti by měli být zbytky Taksitánské armády, možná i nějáká technika.<br></br><img image='hqtabule1.paa' width='320'></img><br></br><br></br>3. Družstvo bude dopraveno Novembrem na severo-východ Zargabádu, kde se nachází sídlo prezidenta Al-Azize. Prezidenta je potřeba zajistit živého a dopravit ho zpátky na základnu!<br></br><img image='hqtabule2.paa' width='320'></img><br></br><br></br>Po úspěšném dokončení svých úkolů se jednotky budou hlásit o další rozkazy.
"]];

// ====================================================================================

// NOTES: SITUATION
// The code below creates the situation sub-section of notes.

_sit = player createDiaryRecord ["diary", ["Situace","
<br/>
Území Tákistánu bylo kompletně obsazeno jednotkami NATO. Hlavní představitelé Tákistánské armády byli zajati nebo zneškodněni. Poslední odpor je kladen v hlavním městě, kde se schovává také prezident Al-Aziz.
<br/><br/>
PŘÁTELSKÉ JEDNOTKY
<br/>
V oblasti se budou pohybovat čtyři družstva - dvě pěchotní družstva, družstvo speciálních jednotek a podpůrné družstvo Yankee. K dispozici je také letecká podpora s volacím znakem November. Na severu od města nám budou pomáhat spřátelené jednotky Tákistánského Obyvatelstva.
"]];

// ====================================================================================
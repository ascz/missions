player createDiarySubject ["PO3WB","Patrol Ops WIKI"];
player createDiaryRecord ["PO3WB",["Ambient Patrols",format ["%1%2",
	if( PO3_param_ambientpatrolair ) then { "The enemy have deployed Air Patrols over the area of operations.<br/>Seek out and destroy the enemy JTAC to prevent them from orgainising their AIR FORCES<br/><br/>" }else{""},
	if( PO3_param_ambientpatrolgnd ) then { "The enemy have deployed Ground Patrols over the area.<br/>Seek out and destroy the enemy COMMANDER to prevent them from orgainising further PATROLS<br/><br/>" }else{""}
	]
]];

player createDiarySubject ["PO3WK","Informace"];
player createDiarySubject ["PO3CR","Co je PO3?"];
player createDiaryRecord ["PO3CR",["Titulky","Framework napsaný autorem Eightysix pro Online Combat Battalion Tactical mise<br/><br/>Online Combat Battalion Australia (www.ocb.net.au), jejich podpora a testování bylo klíčové pro tuto misi.<br/>- [OCB]Dash za kód, za inspiraci a pomoc s vývojem děkuji<br/><br/>
Překladatelé:<br/>
- Čeština: Baki, Evro<br/>
- Francouština: GranolaBar<br/>
- Němčina: Senshi<br/>
- Polština: Rydygier<br/>
- Portugalština: Caico1983<br/>
- Španělština: BIG (Armaholic)<br/>
<br/>Speciální poděkování patří:<br/>
- Bohemia Interactive Stuidos za Arma sérii, kód a funkce<br/>
- BON_Inf za kód a inspiraci z jeho misí a scriptů<br/>
- Shuko for za kód a inspiraci z jeho úžasného Task Systemu a poziconálních Scriptů<br/>
- KillZoneKid za kód a inspiraci z jeho tutoriálů<br/>
- Kegety za jeho Spectator scripty<br/>
- XENO for za kód a inspiraci z Domination a za nastavení tak vysokého standartu<br/>
- R3F za inspiraci z jejich Arma 2 Logistické Scripty<br/>
- Tonic za jeho Virtual Ammobox System<br/>
- Tajin za Scripty Náhlavní kamery<br/>
- aeroson za jeho pokročilé loadout scripts<br/>
- cobra4v320 za jeho HALO scripty<br/>
- Kronzky za jeho string library funkce<br/>
- SaMatra za pomoc s UI prvky a Ruský překlad<br/>
- Dslyecxi za jeho Paper doll systém.<br/>
- Každému z Arma komunity za podporu, inspiraci a prvky které pomohli k vývoji tohoto frameworku"]];

player createDiaryRecord ["PO3CR",["Verze",format["MultiPlayer Scripting Framework<br/>Version %1",localize "STR_PO3_VERSION"]]];

player createDiaryRecord ["PO3WK",["Úkoly","
Systém úkol§<br/>
Stisknutím ""J"" nebo položkou Úkoly v mapě.<br/
Toto vám zobrazí všechny úkoly přiřazené tobě a tvému týmu s aktuálnim statusem.<br/>
<br/>
<img image='\A3\ui_f\data\map\mapcontrol\taskIcon_ca.paa' width='25px'/> Status: Aktivní - Barva: Bílá<br/>
<img image='\A3\ui_f\data\map\mapcontrol\taskIconCanceled_ca.paa' width='25px'/> Status: Zrušeno - Colour: Šedá<br/>
<img image='\A3\ui_f\data\map\mapcontrol\taskIconFailed_ca.paa' width='25px'/> Status: Neúspěch - Colour: Červená<br/>
<img image='\A3\ui_f\data\map\mapcontrol\taskIconDone_ca.paa' width='25px'/> Status: Úspěch - Colour: Zelená<br/>"]];

//player createDiaryRecord ["PO3WK",["Podpora","Na žádostích o podporu se teprve pracuje"]];

player createDiaryRecord ["PO3WK",["Značky týmů",format["Systém Značek Týmů<br/><br/>Každá přátelská jednotka z vaší strany je označena na mapě jejich určitou ikonou.<br/><br/>Ikona zobrazuje jaké vozidlo skupina s jejich velitelem má apod.<br/><img image='%1' width='25px'/> - Pěchota<br/><img image='%2' width='25px'/> - Obrněná technika<br/><img image='%3' width='25px'/> - Helikoptéra<br/><img image='%4' width='25px'/> - Technika: <br/><img image='%5' width='25px'/> - Vozidla<br/><img image='%6' width='25px'/> - Letadlo<br/><img image='%7' width='25px'/> - Statická zbraň<br/><img image='%8' width='25px'/> - UAV<br/>",["n_inf"] call PO3_fnc_getCfgIcon,["n_armor"] call PO3_fnc_getCfgIcon,["n_air"] call PO3_fnc_getCfgIcon,["n_mech_inf"] call PO3_fnc_getCfgIcon,["n_motor_inf"] call PO3_fnc_getCfgIcon,["n_plane"] call PO3_fnc_getCfgIcon,["n_art"] call PO3_fnc_getCfgIcon,["n_uav"] call PO3_fnc_getCfgIcon] ] ];

player createDiaryRecord ["PO3WK",["Pozorovatel","
Systém pozorovatele<br/>
<br/>
Systém pozorovatele je Script od Kegetyho a je pouze přístupný po smrti.<br/>
Klikni na text kamera/cíl navrhu a otevře se ti kamera/cíl menu.<br/>
Jednotky na mapě mohou být překlikatelné<br/>
<br/>
Ovládání na klávesnici:<br/>
A/D - Předešlý/Následující cíl<br/>
W/S - Předešlá/Následující kamera<br/>
1-5 - Direct camera change<br/>
N - 3D view: Toggle night vision on/off<br/>
N - Full map: Toggle marker text off/names/types<br/>
T - Toggle unit tags on/off<br/>
F - Toggle AI filter on/off<br/>
G - Toggle Group/Formation Leader filter on/off<br/>
H - Toggle Map Markers Updates on/off<br/>
Tab - Toggle UI on/off<br/>
M - Toggle map on/full/off<br/>
Numpad plus/minus - Increase/decrease full map marker size<br/>
Space - Drop camera (W,S,A,D keys = movement)<br/>
Space - Toggle gunsight (1st person view)<br/>
Esc - Butterfly mode<br/>
<br/>
Mouse controls:<br/>
Right button - Rotate camera (free camera mode only)<br/>
Left button - Move camera<br/>
Left and right button - Zoom"]];

player createDiaryRecord ["PO3WK",["Respawn systém","
Respawn systém<br/>
<br/>
Hráči se můžou respawnovat na kterémkoliv respawn bodu pokud bude zabit nebo pokud má dostatek životů k pokračování.<br/>
Hráči si také mohou vytvořit Schromaždiště díky kterému se můžou respawnovat tam kde si Schromaždiště vytvořily.<br/>
Hráči mají taky možnost respawnovat se ve vzduchu při seskoku padákem. Instrukce jsou níže.<br/>
<br/>
The avaiable positions are listed on the left side of the tablet.<br/>
Double click or select & accept a specfic position to redeploy to that location.<br/>
<br/>
Aircraft Vehicles can only be redeployed to if:<br/>
 - The aircraft has enough cargo capacity<br/>
 - The aircraft is greater than 50 metres ABOVE the terrain.<br/>
Sea Vehicles can only be redeployed to if:<br/>
 - The Vehicle has enough cargo capacity<br/>
  - The vehicle is stationary<br/>
Land Vehicles can only be redeployed to if:<br/>
 - The Vehicle has enough cargo capacity<br/>
 - The vehicle is stationary<br/>
<br/>
Players can also redploy by HALO if the mission allows this and they are over the minimum amount of time between jumps.<br/>
To HALO, select HALO from the left menu and double click on a location on the map to begin the jump."]];
/*
player createDiaryRecord ["PO3WK",["NCWS","
Info about the Network Centric Warefare System
"]];
player createDiaryRecord ["PO3WK",["Systém Zranění","
Systém Zranění<br/>
<br/>
If a player has recieved too much damage, they will go into an unconcious state.<br/>
They can be brought to concious again by team mates through the First Aid Interaction,<br/>
however require medical attention before being fully operational again.<br/>
"]];
*/
player createDiaryRecord ["PO3WK",["Logistika","
Systém logistiky<br/>
<br/>
Resupply of a vehicle will repair, refuel and rearm the vehicle back to its full state. This can be done near any object designated as a resupply point.<br/>
</br>
Loading and Transportation.<br/>
Objects can be loaded into containers and vehicles and will remain there until it is unloaded or the vehicle is destroyed. This includes ammoboxes.<br/>
Players can drag lighter objects around allowing them to create bases and move ammo crates to better positions.<br/>
</br>
Air Drops<br/>
Cargo Helicopters and Planes can be loaded with ammoboxes or other vehicles and these can be air dropped over an area at heights greater than 100 metres.<br/>
Cargo Helicopter: Hover over the object and select the action to lift. If there is no action, the object is too heavy.<br/>
Cargo Planes: Move the object to the rear of the plane and load the object inside. The pilot will then have the action to air drop the object when they are above 150m over the terrain or water.<br/>"]];

player createDiaryRecord ["PO3WK",["IED","
IED System<br/>
<br/>
In the latest version, IEDs are dangerous to both Infantry and Vehicles.<br/>
They can be disguised as almost anything, from a rubbish pile, a can or even a whole vehicle.<br/>
<br/>
If you suspect an object is an IED, dismount from the vehicle and approach the object by crawling towards it. This will often prevent any trigger of the IED.<br/>
To disarm the IED, aim at the IED and select ""Disarm IED"" from your action menu.<br/>
If you are not an engineer, you have a risk of failure. This will result in the IED detonating.<br/>
Alternately you can just set a C4 explosive next to it and detonate it once clear.
"]];

player createDiaryRecord ["PO3WK",["HUD","The Player Hud<br/><br/>The hud allows you to set your view distance options, terrain detail and density and toggle the 3D names that appear above each of nearby players.<br/>These options can be disabled by the mission admin."]];
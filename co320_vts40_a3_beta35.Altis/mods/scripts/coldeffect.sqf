private ["_unit"]; 
_unit = _this select 0;
_staerke = 0.1;  //stärke des nebels (0 to 1)

while {alive _unit} do {
	sleep (2 + random 2); // zufällige zeit zwischen den atmezügen

	_source = "logic" createVehicleLocal (getpos _unit);
	_fog = "#particlesource" createVehicleLocal getpos _source;
	_fog setParticleParams [["\A3\data_f\ParticleEffects\Universal\Universal", 16, 12, 13,0],
	"", 
	"Billboard", 
	0.5, 
	0.5, 
	[0,0,0],
	[0, 0.2, -0.2], 
	1, 1.275,	1, 0.2, 
	[0, 0.2,0], 
	[[1,1,1, _staerke], [1,1,1, 0.01], [1,1,1, 0]], 
	[1000], 
	1, 
	0.04, 
	"", 
	"", 
	_source];
	_fog setParticleRandom [2, [0, 0, 0], [0.25, 0.25, 0.25], 0, 0.5, [0, 0, 0, 0.1], 0, 0, 10];
	_fog setDropInterval 0.001;

	_source attachto [_unit,[0,0.15,0], "neck"]; // ungefähre position des mundes

	sleep 0.5; // zeit 0.5 sekunden länge des nebels
	deletevehicle _source;
	deleteVehicle _fog;
};
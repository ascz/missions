if (!isDedicated && (isNull player)) then
{
    waitUntil {sleep 0.1; !isNull player};
};


// ====================================================================================

// NOTES: SITUATION
// The code below creates the situation sub-section of notes.
// <br></br> je nový řádek

_sit = player createDiaryRecord ["diary", ["Situace","

    V Rusku, Murmanské oblasti, vyhlásila banda extrémistu z militantní organizace NAPA válku Rusku. Podařilo se jim obsadit nemalou část oblasti a dostat se k vojenské technice. Nyní jsme připraveni zaútočit a skoncovat se vším co začali.


"]];

// ====================================================================================

// NOTES: MISSION
// The code below creates the mission sub-section of notes.

_mis = player createDiaryRecord ["diary", ["Mise","

    Postupujte podle daných úkolů.

"]];
// NOTES: MISSION
// The code below creates the mission sub-section of notes.

_mis = player createDiaryRecord ["diary", ["Mise","
<br/>
1. a 2. Družstvo má za úkol s podporou Novembra a Zulu obsadit město Bastam a všechny a jeho obrané linie na severo-západ od něj.<br></br><img image='hqtabule1.jpg' width='320'></img><br></br><br></br>3. Družstvo bude dopraveno Novembrem na severo-západ do hor, kam se stáhli nepřátelské jednotky z města Mulladost, má za úkol vyčistit nepřátelské kempy a zneškodnit jejich zásoby, včetně nepřátelských AA postavení.  Po úspěšném vyčištění hor se 3. Družstvo skrze <marker name = 'udoli'>údolí Naygul</marker> dostane do města Nagara kde zneškodní veškeré nepřátelské jednotky.<br></br><img image='hqtabule2.jpg' width='320'></img><br></br><br></br>Po úspěšném dokončení svých úkolů se jednotky přeskupí a zajistí pozice u obrané linie kvůli případnému nepřátelskému proti-útoku z letiště.
"]];

// ====================================================================================

// NOTES: SITUATION
// The code below creates the situation sub-section of notes.

_sit = player createDiaryRecord ["diary", ["Situace","
<br/>
Oblast Garmarud a Garmsar byla zajištěna a nepřítel byl zahnán k obraným liniím u letiště Rasman. Tyto obrané linie je potřeba prorazit a zahnat protivníka až na letiště.
<br/><br/>
PŘÁTELSKÉ JEDNOTKY
<br/>
Podpůrné družstvo Yankee bude útočit na nepřátelské pozice severně od Imaratu. Po úspěšném vyčištění této oblasti bude družstvo Yankee pokračovat na západ podpořit pěchotní jednotky u města Bastam.
"]];

// ====================================================================================